package com.pingpoc.client.data;

/**
 * Created by michael on 20/03/16.
 */
public class ContactRequestObject {
    public Contact request;
    public User requesting_user;
    public ContactRequestObject(Contact request, User requesting_user){
        this.request = request;
        this.requesting_user = requesting_user;
    }
}
