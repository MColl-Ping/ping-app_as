package com.pingpoc.client.data;

public class PingRequest {
	public String email;
	public String username;
	public String source_id;
	public String target_id;
	public String locationx;
	public String locationy;
	public String message;
	public String updated_at;
	public String created_at;
	public String id;
}