package com.pingpoc.client.data;

import java.io.Serializable;

public class PingResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public int request_id;
	public Float locationx;
	public Float locationy;
	public String email;
	public String message;
	public String updated_at;
	public String username;

	public PingResponse(int id, int request_id, Float locationx, Float locationy, String email, String message, String updated_at, String username) {
		this.id = id;
		this.request_id = request_id;
		this.locationx = locationx;
		this.locationy = locationy;
		this.email = email;
		this.message = message;
		this.updated_at = updated_at;
		this.username = username;
	}

}
