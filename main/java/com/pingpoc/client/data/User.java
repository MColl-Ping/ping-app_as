package com.pingpoc.client.data;

/**
 * TODO
 */
public class User {
    int id;
    String email;
    String first_name;
    String last_name;
    String username;
    String gcm_device;
    String created_at;

    public User(String email, String first_name, String last_name, String username) {
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
    }

    String updated_at;

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getDescriptorName() {
        if (first_name != null && last_name != null && !first_name.isEmpty() && !last_name.isEmpty()) {
            return first_name + " " + last_name;
        } else {
            return email;
        }
    }

    public String getUsername() {
        return username;
    }

    public double getGcm_device() {
        return Double.valueOf(gcm_device);
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String toString() {
        return email;
    }

    public boolean filterByNameEmail(String query){
        if(query == null){
            return true;
        }
        if(username != null && username.toLowerCase().contains(query.toLowerCase())){
            return true;
        }
        if(email != null && email.toLowerCase().contains(query.toLowerCase())){
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object otherUser) {
        if (!(otherUser instanceof User)) {
            return false;
        }

        if (email.equals(((User) otherUser).email)) {
            return true;
        } else {
            return false;
        }
    }
}
