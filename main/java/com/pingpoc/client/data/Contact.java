package com.pingpoc.client.data;

/**
 * Created by michael on 29/02/16.
 */
public class Contact {
    int id;
    int source_id;
    int target_id;
    int accepted;
    String created_at;
    String updated_at;

    public int getId() {
        return id;
    }

    public int getSource_id() {
        return source_id;
    }

    public int getTarget_id() {
        return target_id;
    }

    public int getAccepted() {
        return accepted;
    }

    public boolean isAccepted() {
        if (accepted == 1) {
            return true;
        }
        return false;
    }

    public void setAccepted(boolean accept) {
        if (accept) {
            accepted = 1;
        } else {
            accepted = 0;
        }
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
