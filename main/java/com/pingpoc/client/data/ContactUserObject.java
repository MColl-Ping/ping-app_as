package com.pingpoc.client.data;

/**
 * Created by michael on 29/02/16.
 */
public class ContactUserObject {
    public Contact contact;
    public User user;

    public ContactUserObject(Contact contact, User user){
        this.contact = contact;
        this.user = user;
    }
}
