package com.pingpoc.client;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.pingpoc.client.data.ContactRequestObject;
import com.pingpoc.client.data.ContactUserObject;
import com.pingpoc.client.data.PingResponse;
import com.pingpoc.client.data.User;

public class ContactManager {

    private static User[] userObjects;

    private static ContactUserObject[] contactUserObjects;

    private static ContactRequestObject[] contactRequestObjects;

    private static HashMap<ContactUserObject, PingResponse> contactResponses;

    public static void setUsers(User[] newUserObjects){
        userObjects = newUserObjects;
    }

    public static void setRequests(ContactRequestObject[] newContactRequestObject) {
        contactRequestObjects = newContactRequestObject;
    }

    public static void setContacts(ContactUserObject[] newContactsUserObject) {
        contactUserObjects = newContactsUserObject;
    }

    public static ContactUserObject[] getContactUserObjects() {
        return contactUserObjects;
    }

    public static ContactUserObject[] getAcceptedContactUserObjects() {
        if (contactUserObjects == null) {
            return new ContactUserObject[0];
        }
        ArrayList<ContactUserObject> result = new ArrayList<>();
        for (ContactUserObject contact : contactUserObjects) {
            if (contact.contact.isAccepted()) {
                result.add(contact);
            }
        }
        return (ContactUserObject[]) result.toArray(new ContactUserObject[result.size()]);
    }

    public static ContactRequestObject[] getContactRequestObjects() {
        if (contactRequestObjects == null) {
            return new ContactRequestObject[0];
        }
        return contactRequestObjects;
    }

    public static void deleteContact(int contact_id) {
        ContactUserObject targetObj = null;
        for (ContactUserObject obj : contactUserObjects) {
            if (obj.contact.getId() == contact_id) {
                targetObj = obj;
                List list = new ArrayList(Arrays.asList(contactUserObjects));
                list.remove(targetObj);
                contactUserObjects = (ContactUserObject[]) list.toArray(new ContactUserObject[list.size()]);
                break;
            }
        }
    }

    public static void deleteRequest(int request) {
        ContactRequestObject targetObj = null;
        for (ContactRequestObject obj : contactRequestObjects) {
            if (obj.request.getId() == request) {
                targetObj = obj;
                List list = new ArrayList(Arrays.asList(contactRequestObjects));
                list.remove(targetObj);
                contactRequestObjects = (ContactRequestObject[]) list.toArray(new ContactRequestObject[list.size()]);
            }
        }
    }

    public static void addContact(ContactUserObject newContact) {
        List list = new ArrayList(Arrays.asList(contactUserObjects));
        if (list.contains(newContact)) {
            return;
        }
        list.add(newContact);
        contactUserObjects = (ContactUserObject[]) list.toArray(new ContactUserObject[list.size()]);
    }

    public static void addRequest(ContactRequestObject newRequest) {
        List list = new ArrayList(Arrays.asList(contactRequestObjects));
        if (list.contains(newRequest)) {
            return;
        }
        list.add(newRequest);
        contactRequestObjects = (ContactRequestObject[]) list.toArray(new ContactRequestObject[list.size()]);
    }

    public static User[] getUsers(){
        if(userObjects == null){
            return new User[0];
        }
        return userObjects;

    }

}
