package com.pingpoc.client.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Address;
import android.location.Geocoder;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.pingpoc.R;
import com.pingpoc.client.activities.SigninActivity;
import com.pingpoc.client.exceptions.UnknownAddressException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class UtilMethods {

    /**
     * Store {@code String} to {@linkplain SharedPreferences}
     */
    public static void storeToSharedPreferences(Context context, String id, String value) {
        final SharedPreferences prefs = context.getSharedPreferences(SigninActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
        Log.d(UtilMethods.class.getName(), "Storing String: " + value + " at Id: " + id);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(id, value);
        editor.commit();
    }

    public static Object fromJson(String json, Class clazz) {
        Gson gson = new Gson();
        return gson.fromJson(json, clazz);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static void displayPromptForEnablingGPS(
            final Context context) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(context);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "Ping requires a location provider to function. Please press OK to enable location services or Quit to exit the app ";

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                context.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Quit",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                                System.exit(0);
                            }
                        });
        builder.create().show();
    }

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static String geoCodeSingleAddressFromCoords(Geocoder geocoder, double lat, double lon) throws UnknownAddressException {
        List<Address> addresses = new ArrayList<Address>();
        Address address;
        String result = "";
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        address = addresses.get(0);
        int maxIndex = address.getMaxAddressLineIndex();
        for (int i = 0; i < maxIndex; i++) {
            String addressLine = address.getAddressLine(i).replaceAll(" ", "+");
            result += addressLine + ", ";
        }

        result = result.substring(0, result.length() - 2);

        if (result == "")
            throw new UnknownAddressException();

        return result;
    }

    public static LatLng geoCodeCoordsFromAddress(Geocoder geocoder, String address) throws UnknownAddressException {
        List<Address> addresses = new ArrayList<Address>();
        Address tempAddress;
        LatLng result;
        try {
            addresses = geocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        tempAddress = addresses.get(0);
        if (tempAddress == null) {
            throw new UnknownAddressException();
        }

        result = new LatLng(tempAddress.getLatitude(), tempAddress.getLongitude());

        return result;
    }


    public static String formatDist(float meters) {
        if (meters < 1000) {
            return ((int) meters) + "m";
        } else if (meters < 10000) {
            return formatDec(meters / 1000f, 1) + "km";
        } else {
            return ((int) (meters / 1000f)) + "km";
        }
    }

    private static String formatDec(float val, int dec) {
        int factor = (int) Math.pow(10, dec);

        int front = (int) (val);
        int back = (int) Math.abs(val * (factor)) % factor;

        return front + "." + back;
    }

    public static String formatTime(Date latestDate, Date earlierDate) {
        long diff = latestDate.getTime() - earlierDate.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        StringBuilder sb = new StringBuilder();
        if (days > 0)
            sb.append(days).append("d");
        if (hours > 0)
            sb.append(hours).append("h");

        sb.append(minutes).append("m");

        return sb.toString();
    }

    public static Date dateFromString(String date, TimeZone timezone) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(timezone);
        Date result = null;
        try {
            result = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static void showToast(String message, Context c) {
        Toast t = Toast.makeText(c, message, Toast.LENGTH_LONG);
        TextView messageView = (TextView) t.getView().findViewById(android.R.id.message);
        messageView.setBackgroundColor(c.getResources().getColor(android.R.color.transparent));
        t.show();
    }

    public static String stringTimeDifference(Date startDate) {
        Date endDate = new Date();

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;


        String dayString = elapsedDays > 0 ? elapsedDays + "d " : "";

        String hourString = elapsedDays == 0 && elapsedHours == 0 ? "" : elapsedHours + "h ";

        String minuteString = elapsedDays == 0 & elapsedHours == 0 && elapsedMinutes == 0 ? "" : elapsedMinutes + "m ";

        return dayString + hourString + minuteString + elapsedSeconds + "s ago";
    }
}
