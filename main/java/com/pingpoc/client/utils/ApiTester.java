package com.pingpoc.client.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.GET;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pingpoc.R;

public class ApiTester extends Activity {
	
	private ListView parametersList;
	
	private TextView responseText;
	
	private ArrayList<Parameter> parameters;
	
	private ParameterArrayAdapter listAdapter;
	
	private RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint("http://api-beta.breezometer.com/")
				.build();
	
	BreezoInterface breezoApi;

	@Override 
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_api_test);
		
		breezoApi = restAdapter.create(BreezoInterface.class);
		
		parameters = new ArrayList<Parameter>();
		
		listAdapter = new ParameterArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1,parameters);
		
		Button addParameterButton = (Button) findViewById(R.id.api_tester_Button_add_parameter);
		Button removeParameterButton = (Button) findViewById(R.id.api_tester_Button_remove_parameter);
		Button sendRequestButton = (Button) findViewById(R.id.api_tester_Button_send);
		
		addParameterButton.setOnClickListener(new AddParameterListener());
		removeParameterButton.setOnClickListener(new RemoveParameterListener());
		sendRequestButton.setOnClickListener(new SendRequestButtonListener());
		
		parametersList = (ListView)findViewById(R.id.api_tester_LinearLayout_parameters);
		responseText = (TextView)findViewById(R.id.api_tester_TextView_response);
		
		parameters.add(new Parameter(createParameterEntry()));
		
		parametersList.setAdapter(listAdapter);
	}
	
	private View createParameterEntry(){
		LinearLayout newLayout = new LinearLayout(getApplicationContext());
		EditText keyEditText = new EditText(getApplicationContext());
		EditText valueEditText = new EditText(getApplicationContext());
		
		//0 = HORIZONTAL
		newLayout.setOrientation(LinearLayout.HORIZONTAL);

		keyEditText.setHint("key..");
		valueEditText.setHint("value..");

		newLayout.addView(keyEditText);
		newLayout.addView(valueEditText);
		
		return newLayout;
	}
	
	private class AddParameterListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			Parameter parameter = new Parameter(createParameterEntry());
			parameters.add(parameter);
			listAdapter.notifyDataSetChanged();
		}
		
	}
	
	private class RemoveParameterListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			parameters.remove(parameters.size()-1);
			listAdapter.notifyDataSetChanged();
		}
	}
	
	private class SendRequestButtonListener implements OnClickListener{
		@SuppressWarnings("unchecked")
		@Override
		public void onClick(View v) {
			new AsyncTask() {
				Response response;
				 @Override
		            protected Object doInBackground(final Object ... params ) {
					 	try{
//					 		response = breezoApi.getAirQuality("40.7324296", "-73.9977264", "eab95271c3bd4b01a0a464c27376f0e4");
					 		response = breezoApi.getTest();
					 	}catch(RetrofitError err){
					 		err.printStackTrace();
					 	}
						return null;
		            }

		            @Override
		            protected void onPostExecute( final Object result ) {
	            		responseText.setText(responseToString(response));
//	            		Toast.makeText(getApplicationContext(), response.getBody().toString(), Toast.LENGTH_SHORT).show();
		            }
			}.execute();
		}
	}
	
	private class Parameter {
		private View layout;
		
		EditText keyEditText;
		EditText valueEditText;
		
		public Parameter(View layout) {
			this.layout = layout;
			keyEditText = (EditText) layout.findViewById(0);
			valueEditText = (EditText) layout.findViewById(0);
		}
		
		public String getKey() {
			return keyEditText.getText().toString();
		}

		public String getValue() {
			return valueEditText.getText().toString();
		}

		public View getLayout(){
			return this.layout;
		}
	}
	
	private class ParameterArrayAdapter extends ArrayAdapter<Parameter>{

		public ParameterArrayAdapter(Context context, int resource, ArrayList<Parameter> parameters) {
			super(context, resource, parameters);
		}

		@Override
		public View getView(int id, View view, ViewGroup viewgroup){
			return parameters.get(id).getLayout();
		}
		
	}
	
	private interface BreezoInterface {
		@GET("/baqi")
		public Response getAirQuality(@Field("lat") String lat, @Field("lon") String lon, @Field("key")String key);
		
		@GET("/baqi/?lat=40.7324296&lon=-73.9977264&key=eab95271c3bd4b01a0a464c27376f0e4")
		public Response getTest();
	}
	
	public static String responseToString(Response r){
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(r.getBody().in()));
			while((line = reader.readLine()) != null){
				sb.append(line);
				sb.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
}
