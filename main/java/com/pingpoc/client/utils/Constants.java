package com.pingpoc.client.utils;

public class Constants {

	public static String GCM_PREF_KEY = "GCM_REG_KEY";
	public static String SERIALIZATION_KEY = "SERIAL";
	
	public static final String DEV_MODE_USER = "gcm@gcm.com";
	public static final String DEV_MODE_PWD = "gcm";

	public static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;
}
