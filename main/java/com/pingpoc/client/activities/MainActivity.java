package com.pingpoc.client.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.pingpoc.R;
import com.pingpoc.client.fragments.ContactsPingFragment;
import com.pingpoc.client.fragments.MapFragment;
import com.pingpoc.client.fragments.PingRequestsFragment;
import com.pingpoc.client.service.GCMIntentService;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.interfaces.AbstractServiceInterface;
import com.pingpoc.client.service.interfaces.PingApiComponent;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PingApiComponent, AbstractServiceInterface, ContactsPingFragment.TabSelectorInterface {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PingService pingService;
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Bind to the ping service
        PingService.bind(this);
//
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setLogo(R.drawable.ping_logo_clear_24dp);
//        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_black_24dp);
//        setSupportActionBar(toolbar);
//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupIcons();

    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        if (intent.getExtras() != null) {
            int type = (int) intent.getExtras().get("intent-type");
            switch (type) {
                case GCMIntentService.contact_request_intent:
//                    tabLayout.getTabAt(1).select();
                    break;
                case GCMIntentService.ping_request_intent:
//                    tabLayout.getTabAt(2).select();
                    break;
                case GCMIntentService.ping_response_intent:
                    int responseId = Integer.parseInt((String) intent.getExtras().get("response-id"));
                    jumpToResponseOnMap(responseId);
                    break;
            }
        }

        if ("notification-intent".equals(intent.getAction())) {
            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotifyMgr.cancel("request", 001);
            mNotifyMgr.cancel("response", 001);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MapFragment());
        adapter.addFragment(new ContactsPingFragment());
        adapter.addFragment(new PingRequestsFragment());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                adapter.getItem(position).onResume();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.map_icon);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_people_black_36dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_location_on_white_36dp);
//        tabLayout.getTabAt(2).setIcon(R.drawable.ic_place_black_24dp);
    }

    @Override
    public PingService getService() {
        return pingService;
    }

    @Override
    public void setService(PingService service) {
        this.pingService = service;
    }

    @Override
    public Context getContext() {
        return this;
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void jumpToResponseOnMap(int responseId) {
        tabLayout.getTabAt(0).select();
        MapFragment mapFragment = ((MapFragment) adapter.getItem(0));
        mapFragment.jumpToResponse(responseId);
    }

    @Override
    public void onBackPressed() {
        //Override to stop going back to login screen
    }

}