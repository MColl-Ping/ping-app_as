package com.pingpoc.client.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pingpoc.R;
import com.pingpoc.client.service.GCMIntentService;
import com.pingpoc.client.service.InstanceIdListenerService;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.PositionService;
import com.pingpoc.client.service.interfaces.PingApi;
import com.pingpoc.client.service.interfaces.PingApiComponent;
import com.pingpoc.client.utils.Constants;
import com.pingpoc.client.utils.UtilMethods;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.pingpoc.client.utils.UtilMethods.showToast;

/**
 * Launch activity that checks log in details, GCM registration etc.
 */
public class SigninActivity extends AppCompatActivity implements PingApiComponent, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    static final int REQUEST_GOOGLE_PLAY_SERVICES = 0;

    static final int REQUEST_SIGNUP = 0;

    int RC_SIGN_IN;

    SharedPreferences settings;

    ProgressDialog progressDialog;

    PingService pingService;

    SharedPreferences prefs;

    @InjectView(R.id.register_text)
    TextView _signupLink;

    @InjectView(R.id.login_button)
    AppCompatButton _signInButton;

    @InjectView(R.id.activity_launch_EditText_email)
    EditText _emailEditText;

    @InjectView(R.id.activity_launch_EditText_password)
    EditText _passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_launch);

        PingService.bind(this);

        ButterKnife.inject(this);

        _signInButton.setOnClickListener(this);

        _signupLink.setOnClickListener(this);

        checkGooglePlayServicesAvailable();

        boolean permissions = checkPermissions();

        if (permissions) {
            startServices();
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        LocationManager localManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!localManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !localManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            UtilMethods.displayPromptForEnablingGPS(this);
        }

        checkGooglePlayServicesAvailable();
    }

    private void startServices() {
        startService(new Intent(this, PingService.class));
        startService(new Intent(this, InstanceIdListenerService.class));
        startService(new Intent(this, GCMIntentService.class).setAction("blah"));
    }

    /**
     * Launch the main map activity
     */
    public void launchApp() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkGooglePlayServicesAvailable();
    }

    // TODO use this to check for google play services

    /**
     * Check that Google Play services APK is installed and up to date.
     */
    private boolean checkGooglePlayServicesAvailable() {
        final int connectionStatusCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }

    /**
     * Called if the device does not have Google Play Services installed.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, SigninActivity.this,
                        REQUEST_GOOGLE_PLAY_SERVICES);
                dialog.show();
            }
        });
    }

    public boolean checkPermissions() {

        int courseLocationCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        int fineLocationCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (courseLocationCheck == PackageManager.PERMISSION_DENIED || fineLocationCheck == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION);
            ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.MY_PERMISSIONS_REQUEST_LOCATION);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    PositionService posManager = PositionService.getSingletonInstance(this);
                    posManager.requestImmediateLocationUpdate();
                    startServices();
                } else {
                    this.finishAffinity();
                }
                return;
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //TODO implement something here
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                if (checkPasswordField(R.id.activity_launch_EditText_password)) {
                    String passwordString = _passwordEditText.getText().toString();
                    String emailString = _emailEditText.getText().toString();
                    attemptLogin(emailString, passwordString);
                    break;
                }
            case R.id.register_text:
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
        }
    }

    private boolean checkPasswordField(int passwordFieldId) {
        String passwordString = _passwordEditText.getText().toString();
        String emailString = _emailEditText.getText().toString();
        ;

        if (passwordString.isEmpty()) {
            showToast("Please enter a password", getBaseContext());
            return false;
        }
        if (passwordString.length() > 15 || passwordString.length() < 5) {
            showToast("Please enter a password between 5 - 15 characters in length", getBaseContext());
            return false;
        }
        return true;
    }

    @Override
    public void setService(PingService service) {
        this.pingService = service;
        String userEmail = prefs.getString("email", null);
        String userPassword = prefs.getString("password", null);
        if (userEmail != null && userPassword != null) {
            _emailEditText.setText(userEmail);
            _passwordEditText.setText(userPassword);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    private void attemptLogin(final String email, final String password) {
        progressDialog.show();

        pingService.loginToServer(new PingApi.ServerResponseConnector() {
            @Override
            public void onServerResponse(PingApi.ServerResponse response) {
                if (response.hasError()) {
                    progressDialog.dismiss();
                    showToast(response.getMessage(), getBaseContext());
                } else {
                    progressDialog.dismiss();
                    prefs.edit().putString("email", email).commit();
                    prefs.edit().putString("password", password).commit();
                    pingService.getUsers();
                    pingService.getPingResponses();
                    pingService.getPingRequests();
                    pingService.getContacts();
                    launchApp();
                }
            }
        }, email, password);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        PingService.unBind(this);
    }
}
