package com.pingpoc.client.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pingpoc.R;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.interfaces.PingApi;
import com.pingpoc.client.service.interfaces.PingApiComponent;
import com.pingpoc.client.utils.UtilMethods;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.pingpoc.client.utils.UtilMethods.showToast;

/**
 * Created by michael on 6/08/16.
 */
public class RegisterActivity extends AppCompatActivity implements PingApiComponent {

    @InjectView(R.id.activity_register_EditText_username)
    EditText _usernameText;
    @InjectView(R.id.activity_register_EditText_email)
    EditText _emailText;
    @InjectView(R.id.activity_register_EditText_password)
    EditText _passwordText;
    @InjectView(R.id.register_button)
    Button _signupButton;
    @InjectView(R.id.login_text)
    TextView _loginLink;

    PingService pingService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        PingService.bind(this);
        ButterKnife.inject(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void register() {
        if (!validate()) {
            onRegisterFailed();
            return;
        }

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this
        );
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        final String username = _usernameText.getText().toString();
        final String email = _emailText.getText().toString();
        final String password = _passwordText.getText().toString();


        pingService.registerWithServer(new PingApi.ServerResponseConnector() {
            @Override
            public void onServerResponse(PingApi.ServerResponse response) {
                if (response.hasError()) {
                    progressDialog.dismiss();
                    showToast(response.getMessage(), getBaseContext());
                } else {
                    progressDialog.dismiss();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(RegisterActivity.this);
                    prefs.edit().putString("email", email).commit();
                    prefs.edit().putString("password", password).commit();
                    pingService.getPingResponses();
                    pingService.getContacts();
                    pingService.getUsers();
                    launchApp();
                }
            }
        }, email, password, null, null, username);
    }

    public void onRegisterSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onRegisterFailed() {
        showToast("Register failed", getBaseContext());
        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _usernameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _usernameText.setError("at least 3 characters");
            valid = false;
        } else {
            _usernameText.setError(null);
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    @Override
    public void setService(PingService service) {
        this.pingService = service;
    }

    @Override
    public Context getContext() {
        return this;
    }

    /**
     * Launch the main map activity
     */
    public void launchApp() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
