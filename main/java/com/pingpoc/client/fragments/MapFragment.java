package com.pingpoc.client.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.pingpoc.R;
import com.pingpoc.client.PingManager;
import com.pingpoc.client.data.PingResponse;
import com.pingpoc.client.exceptions.UnknownAddressException;
import com.pingpoc.client.service.GCMIntentService;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.PositionService;
import com.pingpoc.client.service.interfaces.AbstractServiceInterface;
import com.pingpoc.client.service.interfaces.PingApi;
import com.pingpoc.client.utils.UtilMethods;
import com.pingpoc.client.wrappers.CustomMapWrapper;

import java.util.HashMap;
import java.util.Locale;

import static com.pingpoc.client.utils.UtilMethods.showToast;

/**
 * Created by michael on 24/04/16.
 */
public class MapFragment extends Fragment {
    View.OnTouchListener sendListener = null;
    PingRequestReciever pingRequestReceiver;
    PingResponseReciever pingResponseReciever;
    AlertDialog.Builder builder;
    GoogleMap map;
    PingService pingService;
    ImageButton mainButton;

    Geocoder geocoder;

    ViewGroup infoWindow;

    CustomMapWrapper mapWrapper;

    HashMap<Marker, Integer> markers = new HashMap<Marker, Integer> ();

    HashMap<Integer, PingResponse> pings = new HashMap<Integer, PingResponse>();

    Context context;

    @Override
    public void onDestroyView() {
        try{
            android.app.FragmentManager fm = getActivity().getFragmentManager();
            android.app.Fragment mapFragment = fm.findFragmentById(R.id.mapview_map);
            if(mapFragment != null){
                fm.beginTransaction().remove(mapFragment).commit();
            }
        }catch(IllegalStateException e){
            e.printStackTrace();
        }
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_map, container, false);

        mapWrapper = (CustomMapWrapper) view.findViewById(R.id.mapLayout);

        android.app.Fragment mapFragment = getActivity().getFragmentManager().findFragmentById(R.id.mapview_map);
        map = ((com.google.android.gms.maps.MapFragment) mapFragment).getMap();

        //Enable myLocation, permissions already checked in start activity.
        //noinspection ResourceType
        map.setMyLocationEnabled(true);

        //Disable maps default toolbar
        map.getUiSettings().setMapToolbarEnabled(false);

        map.setInfoWindowAdapter(new MarkerInfoWindow());

        // MapWrapperLayout initialization
        // 39 - default marker height
        // 20 - offset between the default InfoWindow bottom edge and it's content bottom edge
        mapWrapper.init(map, UtilMethods.getPixelsFromDp(getActivity(), 39 + 20));

        // We want to reuse the info window for all the markers,
        // so let's create only one class member instance
        infoWindow = (ViewGroup) inflater.inflate(R.layout.marker_info_window, null);
        ImageButton infoWindowNavigateButton = (ImageButton) infoWindow.findViewById(R.id.marker_info_window_ImageButton_navigate);
        ImageButton infoWindowDeleteButton = (ImageButton) infoWindow.findViewById(R.id.marker_info_window_ImageButton_delete);
        ImageButton infoWindowStreetViewButton = (ImageButton) infoWindow.findViewById(R.id.marker_info_window_ImageButton_streetview);

        infoWindowDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePingResponse();
            }
        });

        infoWindowNavigateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchNavigation();
            }
        });

        infoWindowStreetViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {launchStreetView(); }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        // TODO check for up to date google play services otherwise map doesn't
        // load, This may be caused only when no google accounts exist

        IntentFilter requestIntentFilter = new IntentFilter();
        requestIntentFilter.addAction(GCMIntentService.PING_REQUEST_ACTION);
        requestIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        pingRequestReceiver = new PingRequestReciever();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(pingRequestReceiver, requestIntentFilter);

        IntentFilter responseIntentFilter = new IntentFilter();
        responseIntentFilter.addAction(GCMIntentService.PING_RESPONSE_ACTION);
        responseIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        pingResponseReciever = new PingResponseReciever();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(pingResponseReciever, responseIntentFilter);

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };

        builder = new AlertDialog.Builder(getActivity())
                .setNegativeButton("Decline", dialogClickListener);

        builder.setTitle("Ping received");
    }

    public void animateMapCamera(GoogleMap map, float lat, float lon, int duration, int zoom) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(map.getCameraPosition().target)      // Sets the center of the map to Mountain View
                .zoom(2)                   // Sets the zoom
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 500, null);
        cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lon))      // Sets the center of the map to Mountain View
                .zoom(zoom)                   // Sets the zoom
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), duration, null);
    }


    private class PingRequestReciever extends BroadcastReceiver implements PingApi.ServerResponseConnector {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String email = intent.getStringExtra("email");
            final int requestId = intent.getIntExtra("request_id", 0);
            builder.setMessage(email);
            builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    acceptPing(Integer.toString(requestId));
                }
            });
            builder.show();
        }

        private void acceptPing(String requestId) {
            Location local = PositionService.getSingletonInstance(getActivity()).getCachedLocation();
            pingService.pingResponse(this, requestId, Double.toString(local.getLatitude()), Double.toString(local.getLongitude()), null);
        }

        @Override
        public void onServerResponse(PingApi.ServerResponse response) {
            // TODO Auto-generated method stub
        }

    }

    private class PingResponseReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            float lat = (float) intent.getDoubleExtra("latitude", 0);
            float lon = (float) intent.getDoubleExtra("longitude", 0);

            //Check this is the current activity
            if(getActivity() != null){
                showToast("Ping Accepted", getActivity());
            }

            updatePingResponses();

            animateMapCamera(map, lat, lon, 1000, 14);
        }

    }

    private void drawResponse(PingResponse response, Context context) {
        IconGenerator generator = new IconGenerator(context);
        TextView iconView = new TextView(getActivity());
        iconView.setBackgroundColor(getResources().getColor(R.color.ping_white));
        iconView.setTextColor(getResources().getColor(R.color.ping_black));
        iconView.setText(response.username);
        iconView.setPadding(10,10,10,10);
        generator.setContentView(iconView);
        Bitmap blah = generator.makeIcon();
        BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(blah);
        MarkerOptions m = new MarkerOptions();
        m.position(new LatLng(response.locationx, response.locationy));
        Marker marker = map.addMarker(m);
        marker.setIcon(descriptor);
        pings.put(response.id, response);
        markers.put(marker, response.id);
    }

    private class MarkerInfoWindow implements GoogleMap.InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            mapWrapper.setMarkerWithInfoWindow(marker, infoWindow);
            return infoWindow;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        pingService = ((AbstractServiceInterface) getActivity()).getService();
        map.clear();
        context = getActivity();
        updatePingResponses();
        resetMapCamera(map);
    }

    private void resetMapCamera(GoogleMap map) {
        Location currentLocation = PositionService.getSingletonInstance(getActivity()).getCachedLocation();
        if (currentLocation == null) return;
        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(currentLocation
                .getLatitude(), currentLocation.getLongitude())));
        map.moveCamera(CameraUpdateFactory.zoomTo(12));
    }

    public void updatePingResponses() {
        map.clear();
        PingResponse[] responses = PingManager.getPingResponses();
        for (PingResponse responseObject : responses) {
            drawResponse(responseObject, context);
        }
    }

    private void deletePingResponse() {
        final Marker marker = mapWrapper.getMarker();
        int responseId = markers.get(marker);
        pingService.removePingResponse(new PingApi.ServerResponseConnector() {
            @Override
            public void onServerResponse(PingApi.ServerResponse response) {
                Integer responseId = markers.get(marker);
                pings.remove(responseId);
                markers.remove(marker);
                marker.remove();
            }
        }, String.valueOf(responseId));
    }

    private void launchStreetView() {
        LatLng markerPos = mapWrapper.getMarker().getPosition();
        String address = null;
        try {
            address = UtilMethods.geoCodeSingleAddressFromCoords(geocoder, markerPos.latitude, markerPos.longitude);
            markerPos = UtilMethods.geoCodeCoordsFromAddress(geocoder, address);
        } catch (UnknownAddressException e) {
            e.printStackTrace();
        }
        Uri gmmIntentUri = Uri.parse("google.streetview:cbll=" + markerPos.latitude + "," + markerPos.longitude);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    private void launchNavigation() {
        LatLng markerPos = mapWrapper.getMarker().getPosition();
        //Jump straight to navigation, we may or may not decide to use this by default in the future.
        //                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + markerPos.latitude + "," + markerPos.longitude);
        //                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        //                mapIntent.setPackage("com.google.android.apps.maps");
        //                MapActivity.this.startActivity(mapIntent);
        String address = null;
        try {
            address = UtilMethods.geoCodeSingleAddressFromCoords(geocoder, markerPos.latitude, markerPos.longitude);
        } catch (UnknownAddressException e) {
            e.printStackTrace();
        }
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + address);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void jumpToResponse(int responseId){
        PingResponse response = pings.get(responseId);
        if(response != null){
            animateMapCamera(map, response.locationx, response.locationy, 1000, 14);
        }
    }

}
