package com.pingpoc.client.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.pingpoc.R;
import com.pingpoc.client.ContactManager;
import com.pingpoc.client.data.Contact;
import com.pingpoc.client.data.User;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.interfaces.AbstractServiceInterface;
import com.pingpoc.client.service.interfaces.PingApi;
import com.pingpoc.client.service.interfaces.PingApi.ServerResponse;
import com.pingpoc.client.service.interfaces.PingApi.ServerResponseConnector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.pingpoc.client.utils.UtilMethods.showToast;

public class AddContactFragment extends AppCompatDialogFragment{
    PingService pingService;

    ContactListAdapter contactAdapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle("Add Contact");
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        pingService.getUsers();
        contactAdapter.clear();
        contactAdapter.addAll(Arrays.asList(ContactManager.getUsers()));
        contactAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_contact_fragment, null);
        SearchView searchView = (SearchView) view.findViewById(R.id.add_contact_fragment_SearchView_search);
        searchView.setOnQueryTextListener(new ContactSearchViewQueryTextListener());
        searchView.setIconifiedByDefault(false);
        searchView.setIconified(false);
        contactAdapter = new ContactListAdapter(getActivity(), 1);
        ListView list = (ListView) view.findViewById((R.id.add_contact_fragment_List));
        list.setAdapter(contactAdapter);
        pingService = ((AbstractServiceInterface) getActivity()).getService();

        return view;
    }

    private class ContactListAdapter extends ArrayAdapter<User> {
        private Context context;
        private int resource = R.layout.contact_list_item_no_checkbox;

        public ContactListAdapter(Context context, int resource) {
            super(context, resource);
            super.addAll(Arrays.asList(ContactManager.getUsers()));
            this.context = context;
        }

        @Override
        public View getView(int position,
                            View convertView,
                            ViewGroup parent) {
            LayoutInflater inflater =
                    ((Activity) context).getLayoutInflater();
            final User user = getItem(position);
            if (user == null) {
                return super.getView(position, convertView, parent);
            }

            View row = inflater.inflate(resource, parent, false);

            TextView contactName = (TextView) row.findViewById(R.id.contact_list_item_Name);
            contactName.setText(user.getUsername());

            TextView contactEmail = (TextView) row.findViewById(R.id.contact_list_item_Email);
            contactEmail.setText(user.getEmail());

            ImageView contactProfile = (ImageView) row.findViewById(R.id.contact_list_item_profileimage);
            View layout = row.findViewById(R.id.contact_list_item_LinearLayout_main);
            layout.setOnClickListener(new LayoutOnClickListener(user));

            return row;
        }

        @Override
        public Filter getFilter(){
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    ArrayList result = new ArrayList();
                    for(User u : ContactManager.getUsers()){
                        if(u.filterByNameEmail(constraint.toString())){
                            result.add(u);
                        }
                    }
                    FilterResults results = new FilterResults();
                    results.values = result;
                    results.count = result.size();
                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    contactAdapter.clear();
                    contactAdapter.addAll((ArrayList)results.values);
                    notifyDataSetChanged();
                }
            };
        }
    }

    private class ContactSearchViewQueryTextListener implements SearchView.OnQueryTextListener, PingApi.UserResponseConnector {

        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            contactAdapter.getFilter().filter(newText);
            return true;
        }


        @Override
        public void onServerResponse(PingApi.UserResponse response) {
            contactAdapter.add(response.getUser());
            contactAdapter.notifyDataSetChanged();
        }
    }

    private class LayoutOnClickListener implements View.OnClickListener {

        User user;

        public LayoutOnClickListener(User user) {
            this.user = user;
        }


        @Override
        public void onClick(View v) {
           AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Contact");
            builder.setMessage("Are you sure you want to send a contact request to " + user.getUsername());
            builder.setPositiveButton("Ok", new AddContactButtonListener(user));
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }
    }

    private class AddContactButtonListener implements DialogInterface.OnClickListener, ServerResponseConnector {

        User u;

        public AddContactButtonListener(User u){
            this.u = u;
        }

        @Override
        public void onServerResponse(ServerResponse response) {
            showToast(response.getMessage(), getActivity());
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            pingService.addContact(this, u.getEmail());
        }
    }

}
