package com.pingpoc.client.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pingpoc.R;
import com.pingpoc.client.ContactManager;
import com.pingpoc.client.data.Contact;
import com.pingpoc.client.data.ContactRequestObject;
import com.pingpoc.client.data.ContactUserObject;
import com.pingpoc.client.data.User;
import com.pingpoc.client.service.GCMIntentService;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.interfaces.AbstractServiceInterface;
import com.pingpoc.client.service.interfaces.PingApi;

import java.util.ArrayList;
import java.util.Arrays;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import static com.pingpoc.client.utils.UtilMethods.showToast;

/**
 * Created by michael on 22/03/16.
 */
public class ContactRequestsFragment extends AppCompatDialogFragment {

    PingService pingService;

    ContactRequestsListAdapter contactAdapter;

    ContactRequestReciever contactRequestReciever;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle("Contact Requests");
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        NotificationManager mNotifyMgr =
                (NotificationManager) getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);
        mNotifyMgr.cancel("contact-request", 001);
        IntentFilter requestIntentFilter = new IntentFilter();
        requestIntentFilter.addAction(GCMIntentService.CONTACT_REQUEST_ACTION);
        requestIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        contactRequestReciever = new ContactRequestReciever();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(contactRequestReciever, requestIntentFilter);

        updateList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contacts_requests_fragment, null);
        pingService = ((AbstractServiceInterface) getActivity()).getService();

        contactAdapter = new ContactRequestsListAdapter(getActivity(), 1, ContactManager.getContactRequestObjects());
        ListView list = (ListView) view.findViewById(R.id.contacts_fragment_ListView);
        list.setAdapter(contactAdapter);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        return view;
    }

    private class ContactRequestsListAdapter extends ArrayAdapter<ContactRequestObject>{
        private Context context;
        private int resource = R.layout.contact_list_item_no_checkbox;

        public ContactRequestsListAdapter(Context context, int resource, ContactRequestObject[] objects) {
            super(context, resource, new ArrayList<ContactRequestObject>(Arrays.asList(objects)));
            this.context = context;
        }

        @Override
        public View getView(int position,
                            View convertView,
                            ViewGroup parent) {

            LayoutInflater inflater =
                    ((Activity) context).getLayoutInflater();
            ContactRequestObject contactRequestObject = getItem(position);
            User user = contactRequestObject.requesting_user;
            Contact contact = contactRequestObject.request;

            View row = inflater.inflate(resource, parent, false);

            TextView contactName = (TextView) row.findViewById(R.id.contact_list_item_Name);
            contactName.setText(user.getUsername());

            TextView contactEmail = (TextView) row.findViewById(R.id.contact_list_item_Email);
            contactEmail.setText(user.getEmail());

            ImageView contactProfile = (ImageView) row.findViewById(R.id.contact_list_item_profileimage);

            View layout = row.findViewById(R.id.contact_list_item_LinearLayout_main);
            layout.setOnClickListener(new OnContactRequestClickListener(user, contact));

            return row;
        }
    }

    private class OnDeleteRequestButtonListener implements View.OnClickListener{

        Contact contact;
        User user;

        public OnDeleteRequestButtonListener(User user, Contact contact){
            this.contact = contact;
            this.user = user;
        }

        @Override
        public void onClick(View v) {
            pingService.removeContact(new PingApi.ContactsResponseConnector() {
                @Override
                public void onServerResponse(PingApi.ContactsResponse response) {
                    if (response.hasError()) {
                        showToast(response.getMessage(), getActivity());
                    } else {
                        showToast("Deleted " + user.getDescriptorName(),getActivity());
                        ContactManager.deleteRequest(contact.getId());
                        updateList();
                    }
                }
            }, user.getId());
        }
    }

    private class OnContactRequestClickListener implements View.OnClickListener{

        Contact contact;
        User user;

        public OnContactRequestClickListener(User user, Contact contact){
            this.contact = contact;
            this.user = user;
        }

        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.setMessage("Are you sure you want to accept the contact request from " + user.getEmail());
            builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pingService.acceptContact(new PingApi.ServerResponseConnector() {
                        @Override
                        public void onServerResponse(PingApi.ServerResponse response) {
                            if (response.hasError()) {
                                makeText(getActivity(), response.getMessage(), LENGTH_SHORT).show();
                            } else {
                                makeText(getActivity(), "Added " + user.getDescriptorName() + " to contacts", LENGTH_SHORT).show();
                                contact.setAccepted(true);
                                ContactUserObject c = new ContactUserObject(contact, user);
                                ContactManager.addContact(c);
                                ContactManager.deleteRequest(contact.getId());
                                updateList();
                            }
                        }
                    }, contact.getId());
                }
            });
            builder.show();



        }
    }

    private void updateList(){
        contactAdapter.clear();
        contactAdapter.addAll(ContactManager.getContactRequestObjects());
        contactAdapter.notifyDataSetChanged();
    }

    private class ContactRequestReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            pingService.getContacts(new PingApi.ContactsResponseConnector(){

                @Override
                public void onServerResponse(PingApi.ContactsResponse response) {
                    updateList();
                }

            });
        }

    }
}
