package com.pingpoc.client.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pingpoc.R;
import com.pingpoc.client.PingManager;
import com.pingpoc.client.data.PingRequest;
import com.pingpoc.client.service.GCMIntentService;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.PositionService;
import com.pingpoc.client.service.interfaces.AbstractServiceInterface;
import com.pingpoc.client.service.interfaces.PingApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.pingpoc.client.utils.UtilMethods.dateFromString;
import static com.pingpoc.client.utils.UtilMethods.showToast;
import static com.pingpoc.client.utils.UtilMethods.stringTimeDifference;

/**
 * Created by michael on 22/03/16.
 */
public class PingRequestsFragment extends Fragment {

    PingService pingService;

    PingRequestsListAdapter pingAdapter;

    PingRequestReciever pingRequestReciever;

    @InjectView(R.id.ping_request_fragment_swipe_refresh_layout)
    SwipeRefreshLayout refreshLayout;

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter requestIntentFilter = new IntentFilter();
        requestIntentFilter.addAction(GCMIntentService.PING_REQUEST_ACTION);
        requestIntentFilter.addAction(GCMIntentService.PING_RESPONSE_ACTION);
        requestIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        pingRequestReciever = new PingRequestReciever();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(pingRequestReciever, requestIntentFilter);

        updateList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ping_requests_fragment, null);

        ButterKnife.inject(this, view);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pingService.getPingRequests(new PingApi.GenericServerConnector() {
                    @Override
                    public void onServerResponse(Object response) {
                        refresh();
                    }
                });
            }
        });

        pingService = ((AbstractServiceInterface) getActivity()).getService();

        pingAdapter = new PingRequestsListAdapter(getActivity(), 1, PingManager.getPingRequests());
        ListView list = (ListView) view.findViewById(R.id.ping_request_fragment_ListView);
        list.setAdapter(pingAdapter);
        return view;
    }

    private class PingRequestsListAdapter extends ArrayAdapter<PingRequest> {
        private Context context;
        private int resource = R.layout.ping_request_list_item;

        public PingRequestsListAdapter(Context context, int resource, PingRequest[] objects) {
            super(context, resource, new ArrayList<PingRequest>(Arrays.asList(objects)));
            this.context = context;
        }

        @Override
        public View getView(int position,
                            View convertView,
                            ViewGroup parent) {

            LayoutInflater inflater =
                    ((Activity) context).getLayoutInflater();
            final PingRequest pingRequestObject = getItem(position);

            View row = inflater.inflate(resource, parent, false);

            Button acceptButton = (Button) row.findViewById(R.id.ping_request_list_item_accept);

            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Location local = PositionService.getSingletonInstance(getActivity()).getCachedLocation();
                    pingService.pingResponse(new PingApi.ServerResponseConnector() {
                        @Override
                        public void onServerResponse(PingApi.ServerResponse response) {
                            showToast(response.getMessage(), getActivity());
                        }
                    }, pingRequestObject.id, Double.toString(local.getLatitude()), Double.toString(local.getLongitude()), null);
                }
            });

            Button denyButton = (Button) row.findViewById(R.id.ping_request_list_item_deny);

            denyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Location local = PositionService.getSingletonInstance(getActivity()).getCachedLocation();
                    pingService.removePingRequest(new PingApi.ServerResponseConnector() {
                        @Override
                        public void onServerResponse(PingApi.ServerResponse response) {
                            showToast(response.getMessage(), getActivity());
                        }
                    }, pingRequestObject.id);
                }
            });

            TextView contactName = (TextView) row.findViewById(R.id.ping_request_list_item_Name);
            contactName.setText(pingRequestObject.username);

            TextView time = (TextView) row.findViewById(R.id.ping_request_list_item_Time);
            String updated_at = pingRequestObject.updated_at;
            String timeDiff = stringTimeDifference(dateFromString(updated_at, TimeZone.getTimeZone("UTC")));
            time.setText(timeDiff);

            ImageView contactProfile = (ImageView) row.findViewById(R.id.ping_request_list_item_profileimage);

            return row;
        }
    }

    private void updateList() {
        pingAdapter.clear();
        pingAdapter.addAll(PingManager.getPingRequests());
        pingAdapter.notifyDataSetChanged();
    }

    private void refresh() {
        refreshLayout.setRefreshing(false);
        updateList();
    }

    private class PingRequestReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            pingService.getPingRequests(new PingApi.GenericServerConnector<PingApi.PingRequestsObject>() {
                @Override
                public void onServerResponse(PingApi.PingRequestsObject response) {
                    updateList();
                }
            });
        }

    }

}
