package com.pingpoc.client.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pingpoc.R;
import com.pingpoc.client.ContactManager;
import com.pingpoc.client.PingManager;
import com.pingpoc.client.data.Contact;
import com.pingpoc.client.data.ContactUserObject;
import com.pingpoc.client.data.PingResponse;
import com.pingpoc.client.data.User;
import com.pingpoc.client.service.GCMIntentService;
import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.PositionService;
import com.pingpoc.client.service.ServerTask;
import com.pingpoc.client.service.interfaces.AbstractServiceInterface;
import com.pingpoc.client.service.interfaces.PingApi;
import com.pingpoc.client.utils.UtilMethods;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.pingpoc.client.utils.UtilMethods.dateFromString;
import static com.pingpoc.client.utils.UtilMethods.formatDist;
import static com.pingpoc.client.utils.UtilMethods.showToast;
import static com.pingpoc.client.utils.UtilMethods.stringTimeDifference;

/**
 * Created by michael on 24/04/16.
 */
public class ContactsPingFragment extends Fragment {

    ContactListAdapter contactAdapter;

    ContactResponseReciever contactResponseReciever;

    PingResponseReciever pingResponseReciever;

    @InjectView(R.id.contacts_ping_fragment_ImageButton_requests)
    ImageButton contactRequestsButton;

    @InjectView(R.id.contacts_ping_fragment_ImageButton_settings)
    ImageButton addContactsButton;

    @InjectView(R.id.contacts_fragment_swipe_refresh_layout)
    SwipeRefreshLayout refreshLayout;

    @InjectView(R.id.contacts_ping_fragment_ListView)
    ListView list;

    TextView contactDistance;

    TextView contactTime;

    Context context;

    PingService pingService;

    AlertDialog.Builder builder;

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

    private PingService getPingService() {
        return ((AbstractServiceInterface) getActivity()).getService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();

        pingService = ((AbstractServiceInterface) getActivity()).getService();

        contactAdapter = new ContactListAdapter(context, 1, ContactManager.getAcceptedContactUserObjects(), PingManager.getPingResponses());

        View view = inflater.inflate(R.layout.contacts_ping_fragment, container, false);

        ButterKnife.inject(this, view);

        list.setAdapter(contactAdapter);

        addContactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAddContacts();
            }
        });

        contactRequestsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchContactRequests();
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new ServerTask(new RefreshServerUpdateTask()) {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        getPingService().getContacts();
                        getPingService().getPingResponses();
                        return null;
                    }
                }.execute(null);
            }
        });

        builder = new AlertDialog.Builder(getActivity())
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.setTitle("Delete Contact");

        return view;
    }

    private class ContactListAdapter extends ArrayAdapter<Pair<ContactUserObject, PingResponse>> {
        private Context context;
        private int resource = R.layout.contact_ping_list_item;

        public ContactListAdapter(Context context, int resource, ContactUserObject[] contactUserObjects, PingResponse[] responses) {
            super(context, resource, new ArrayList<Pair<ContactUserObject, PingResponse>>(matchContactsToResponses(contactUserObjects, responses)));
            this.context = context;
        }

        @Override
        public View getView(int position,
                            View convertView,
                            ViewGroup parent) {
            LayoutInflater inflater =
                    ((Activity) context).getLayoutInflater();
            Pair<ContactUserObject, PingResponse> pair = getItem(position);
            ContactUserObject userContactObject = pair.first;
            final PingResponse responseObject = pair.second;


            final User user = userContactObject.user;
            final Contact contact = userContactObject.contact;

            View row = inflater.inflate(resource, parent, false);

            TextView contactName = (TextView) row.findViewById(R.id.contact_ping_list_item_Name);
            contactName.setText(user.getUsername());

            ImageView contactProfile = (ImageView) row.findViewById(R.id.contact_list_item_profileimage);

            contactDistance = (TextView) row.findViewById(R.id.contact_ping_list_item_Distance);


            contactTime = (TextView) row.findViewById(R.id.contact_ping_list_item_Time);

            final Location currentLocation = PositionService.getSingletonInstance(getActivity()).getCachedLocation();

            if (responseObject != null) {
                Location responseLocation = new Location("");
                responseLocation.setLatitude(responseObject.locationx);
                responseLocation.setLongitude(responseObject.locationy);
                float distanceTo = responseLocation.distanceTo(currentLocation);
                contactDistance.setText(formatDist(distanceTo)+ " away");

                String updated_at = responseObject.updated_at;
                String timeDiff = stringTimeDifference(dateFromString(updated_at, TimeZone.getTimeZone("UTC")));
                contactTime.setText(timeDiff);
            }

            LinearLayout mainLayout = (LinearLayout) row.findViewById(R.id.contact_ping_list_item_LinearLayout_main);
            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (responseObject != null) {
                        ((TabSelectorInterface) getActivity()).jumpToResponseOnMap(responseObject.id);
                    }
                }
            });

            mainLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    launchDeleteContactDialog(user, contact);
                    return true;
                }
            });

            Button requestButton = (Button) row.findViewById(R.id.contact_ping_list_item_request);
            requestButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getPingService().pingRequest(new PingApi.ServerResponseConnector() {
                        @Override
                        public void onServerResponse(PingApi.ServerResponse response) {
                            if(response.hasError()){
                                showToast(response.getMessage(), getActivity());
                            }
                        }
                    }, user.getId(), String.valueOf(currentLocation.getLatitude()), String.valueOf(currentLocation.getLongitude()), null);
                    showToast("Ping sent", getActivity());
                }
            });


            return row;
        }

        private void launchDeleteContactDialog(final User user, final Contact contact) {
            builder.setMessage("Are you sure you want to delete " + user.getEmail());
            builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getPingService().removeContact(new PingApi.ContactsResponseConnector() {
                        @Override
                        public void onServerResponse(PingApi.ContactsResponse response) {
                            ContactManager.deleteContact(contact.getId());
                            refreshImmediate();
                            showToast(response.getMessage(), getActivity());
                        }
                    }, user.getId());
                }
            });
            builder.show();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        context = getActivity();
        pingService = ((AbstractServiceInterface) getActivity()).getService();
        IntentFilter requestIntentFilter = new IntentFilter();
        requestIntentFilter.addAction(GCMIntentService.CONTACT_RESPONSE_ACTION);
        requestIntentFilter.addAction(GCMIntentService.CONTACT_DELETED_ACTION);
        requestIntentFilter.addAction(GCMIntentService.CONTACT_REQUEST_ACTION);
        requestIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        contactResponseReciever = new ContactResponseReciever();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(contactResponseReciever, requestIntentFilter);

        IntentFilter pingRequestIntentFilter = new IntentFilter();
        requestIntentFilter.addAction(GCMIntentService.PING_RESPONSE_ACTION);

        pingResponseReciever = new PingResponseReciever();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(pingResponseReciever, requestIntentFilter);

        refreshImmediate();

    }

    private void refreshImmediate() {
        if (ContactManager.getContactRequestObjects().length != 0) {
            contactRequestsButton.setVisibility(View.VISIBLE);
            contactRequestsButton.setClickable(true);
        } else {
            contactRequestsButton.setVisibility(View.INVISIBLE);
            contactRequestsButton.setClickable(false);
        }
        contactAdapter.clear();
        contactAdapter.addAll(matchContactsToResponses(ContactManager.getAcceptedContactUserObjects(), PingManager.getPingResponses()));
        contactAdapter.notifyDataSetChanged();
    }

    private class ContactResponseReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            refreshImmediate();
        }

    }

    private class PingResponseReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            refreshImmediate();
        }

    }

    private void launchAddContacts() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        new AddContactFragment().show(ft, "addcontact");
    }

    private void launchContactRequests() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        new ContactRequestsFragment().show(ft, "contactrequests");
    }

    public List<Pair<ContactUserObject, PingResponse>> matchContactsToResponses(ContactUserObject[] contactUserObjects, PingResponse[] responses) {
        List<Pair<ContactUserObject, PingResponse>> contactsAndResponses = new ArrayList<Pair<ContactUserObject, PingResponse>>();
        for (ContactUserObject contactUser : contactUserObjects) {
            boolean matched = false;
            for (PingResponse response : responses) {
                if (response.email.equals(contactUser.user.getEmail())) {
                    contactsAndResponses.add(new Pair(contactUser, response));
                    matched = true;
                    break;
                }
            }
            if (!matched) {
                contactsAndResponses.add(new Pair(contactUser, null));
            }
        }
        return contactsAndResponses;
    }

    public interface TabSelectorInterface {
        public void jumpToResponseOnMap(int responseId);
    }

    private class RefreshServerUpdateTask implements PingApi.GenericServerConnector {

        @Override
        public void onServerResponse(Object response) {
            refreshImmediate();
            refreshLayout.setRefreshing(false);
        }
    }

}
