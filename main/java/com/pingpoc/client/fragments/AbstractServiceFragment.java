package com.pingpoc.client.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.pingpoc.client.service.PingService;
import com.pingpoc.client.service.interfaces.AbstractServiceInterface;

public abstract class AbstractServiceFragment extends Fragment{

	/**
	 * Service reference to be retrieved from the parent activity
	 */
	PingService pingService;
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
        
        try {
            pingService = ((AbstractServiceInterface) activity).getService();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AbstractServiceInterface");
        }
	}
	
}
