package com.pingpoc.client;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.pingpoc.client.data.ContactRequestObject;
import com.pingpoc.client.data.ContactUserObject;
import com.pingpoc.client.data.PingRequest;
import com.pingpoc.client.data.PingResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by michael on 30/04/16.
 */
public class PingManager {

    private static PingResponse[] pingResponses;

    private static PingRequest[] pingRequests;

    public static void setPingRequests(PingRequest[] pingRequestsIn) {
        pingRequests = pingRequestsIn;
    }

    public static void setPingResponses(PingResponse[] pingResponsesIn) {
        pingResponses = pingResponsesIn;
    }

    public static PingRequest[] getPingRequests() {
        return pingRequests != null ? pingRequests : new PingRequest[0];
    }


    public static PingResponse[] getPingResponses() {
        return pingResponses != null ? pingResponses : new PingResponse[0];
    }

}
