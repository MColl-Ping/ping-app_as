package com.pingpoc.client.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.pingpoc.R;

/*Position Manager class used to periodically update the values stored for the current position
 * and to allow easy access of this data*/
public class PositionService extends Service implements LocationListener {

    private LocationManager localManager;

    private Location local = null;
    private Context mContext = null;

    private static PositionService singletonInstance = null;

    int timeBetweenUpdates;
    int distanceBetweenUpdates;

    static long TWO_MINUTES = R.integer.ONE_SECOND_IN_NANO_SECONDS * 120L;

    private PositionService(Context context) {
        super();
        this.mContext = context;

        Resources res = context.getResources();
        timeBetweenUpdates = res.getInteger(R.integer.TIME_BETWEEN_UPDATES);
        distanceBetweenUpdates = res.getInteger(R.integer.DISTANCE_BETWEEN_UPDATES);

        localManager = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);

        //Enable GPS location updates if enabled
        localManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                timeBetweenUpdates,
                distanceBetweenUpdates, this);

        //Enable NETWORK location updates if enabled
        localManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                timeBetweenUpdates,
                distanceBetweenUpdates, this);

        local = getCurrentLocation();

    }

    public static synchronized PositionService getSingletonInstance(Context context) {
        if (singletonInstance == null)
            singletonInstance = new PositionService(context);

        //Update the context to the correct applicationContext
        singletonInstance.mContext = context;

        return singletonInstance;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public void onLocationChanged(Location location) {
        local = getBestFix(location, local);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    //Network settings on or off
    public void onProviderEnabled(String provider) {
        //Re-enable location updates
        localManager.requestLocationUpdates(
                provider,
                timeBetweenUpdates,
                distanceBetweenUpdates, this);
    }

    public Location getCurrentLocation() {
        Location gpsLocation = null, networkLocation = null;
        try {
            LocationManager locationManager = localManager;

            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isNetworkEnabled) {
                Log.d("Network", "Network");
                if (locationManager != null) {
                    networkLocation = localManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    local = getBestFix(networkLocation, local);
                }
            }
            if (isGPSEnabled) {
                Log.d("GPS Enabled", "GPS Enabled");
                if (locationManager != null) {
                    gpsLocation = localManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    local = getBestFix(gpsLocation, local);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return local;
    }

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param location            The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    private Location getBestFix(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return location;
        }
        if (location == null) {
            // A valid old location is always better than a non existent new one
            return currentBestLocation;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getElapsedRealtimeNanos() - currentBestLocation.getElapsedRealtimeNanos();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return location;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return currentBestLocation;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return location;
        } else if (isNewer && !isLessAccurate) {
            return location;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return location;
        }
        return currentBestLocation;
    }

    public Location getCachedLocation() {
        if (local != null) {
            return local;
        }

        return getCurrentLocation();
    }


    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    /**
     * Request an immediate location request. This does not guarantee that the local returned by {@link getLastLocation} is
     * up to date as there is a delay between the updates.
     */
    public void requestImmediateLocationUpdate() {
        ImmediateLocationListener immediateLocalListener = new ImmediateLocationListener();
        localManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, immediateLocalListener);
        localManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, immediateLocalListener);
    }

    private class ImmediateLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            if (location.getAccuracy() < R.integer.ACCURACY_IN_METERS) {
                local = location;
                localManager.removeUpdates(this);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
