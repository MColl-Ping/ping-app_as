package com.pingpoc.client.service;

import java.io.IOException;

import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;

public class InstanceIdListenerService extends InstanceIDListenerService{
	
	/**
	 * The Project number as defined by the url of GCM in developer console. TO be used as SenderID
	 */
	protected static final String SENDER_ID = "517359592427";
	
//	protected static final String SENDER_ID = "304038317717";
	
	private static String registrationToken;
	
	private InstanceID instanceID;

	/**
	 * Get a valid registration token from the GCM server. Uses the SENDER_ID (in this case project number)
	 * as a unique identifier.
	 * @return a valid registration token or null
	 */
	public void updateToken(){
		//If we still have a valid reg token then return this.
		if(registrationToken != null){
			return;
		}
		
		//Otherwise request a new one.
		instanceID = InstanceID.getInstance(this);
			new AsyncTask(){
				@Override
				protected Object doInBackground(Object... params) {
					try {
						registrationToken = instanceID.getToken(SENDER_ID,
								GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return null;
				}
			}.execute();
	}
	
	@Override
	public void onTokenRefresh() {
		updateToken();
	    // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
//	    Intent intent = new Intent(this, RegistrationIntentService.class);
//	    startService(intent);
	}
	
	public static String getToken(){
		return registrationToken;
	}
		
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		int service_int = super.onStartCommand(intent, flags, startId);
		updateToken();
		return service_int;
	}
	
}
