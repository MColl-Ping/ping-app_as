package com.pingpoc.client.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pingpoc.client.ContactManager;
import com.pingpoc.client.PingManager;
import com.pingpoc.client.data.ContactRequestObject;
import com.pingpoc.client.data.ContactUserObject;
import com.pingpoc.client.data.User;
import com.pingpoc.client.service.connections.PingServiceConnection;
import com.pingpoc.client.service.interfaces.PingApi;
import com.pingpoc.client.service.interfaces.PingApi.ContactsResponse;
import com.pingpoc.client.service.interfaces.PingApi.ContactsResponseConnector;
import com.pingpoc.client.service.interfaces.PingApi.GenericServerConnector;
import com.pingpoc.client.service.interfaces.PingApi.ServerResponse;
import com.pingpoc.client.service.interfaces.PingApi.ServerResponseConnector;
import com.pingpoc.client.service.interfaces.PingApiComponent;

import java.util.HashMap;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

public class PingService extends Service {

    static final String API_ROOT = "http://128.199.245.30/";

    static HashMap<PingApiComponent, PingServiceConnection> connections = new HashMap<PingApiComponent, PingServiceConnection>();

    PingApi pingApi;

    GoogleCloudMessaging gcm;

    RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(API_ROOT)
            .setRequestInterceptor(new ApiKeyInterceptor())
            .build();

    final IBinder mBinder = new PingBinder();

    SharedPreferences prefs;

    public PingService() {
        super();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int k, int a) {
        restAdapter.setLogLevel(RestAdapter.LogLevel.HEADERS_AND_ARGS);
        pingApi = restAdapter.create(PingApi.class);
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return super.onStartCommand(intent, k, a);
    }

    public class PingBinder extends Binder {
        public PingService getService() {
            return PingService.this;
        }
    }

    /**
     * Starts the service that handles registering this device with google cloud messaging
     */
    public void registerWithGCM() {
        startService(new Intent(this, InstanceIdListenerService.class));
    }

    public void registerWithServer(GenericServerConnector<ServerResponse> connector, String email,
                                   String password, String first_name, String last_name, String user_name) {
        //TODO Replace null with optional values at some point
        new RegisterUserTask(connector).execute(email, password, first_name, last_name, user_name);
    }

    public void loginToServer(PingApi.ServerResponseConnector connector, String email, String password) {
        new LoginUserTask(connector).execute(email, password);
    }

    public void getUser(PingApi.UserResponseConnector connector, String email) {
        new GetUserTask(connector).execute(email);
    }

    public void getContacts(GenericServerConnector connector) {
        new GetContactsTask(connector).execute();
    }

    public void getContacts() {
        new GetContactsTaskNoResponse().execute();
    }

    public void getUsers() {
        new GetUsersTaskNoResponse().execute();
    }

    public void pingRequest(ServerResponseConnector connector, int target_id, String latitude, String longitude, String message) {
        new PingRequestTask(connector).execute(String.valueOf(target_id), latitude, longitude, message);
    }

    public void getPingResponses(GenericServerConnector connector) {
        new GetPingResponsesTask(connector).execute();
    }

    public void getPingResponses() {
        new GetPingResponsesTaskNoResponse().execute();
    }

    public void getPingRequests(){
        new GetPingRequestsTaskNoResponse().execute();
    }

    public void getPingRequests(GenericServerConnector connector){
        new GetPingRequestsTask(connector).execute();
    }

    public void pingResponse(PingApi.ServerResponseConnector connector, String request_id, String latitude, String longitude, String message) {
        new PingResponseTask(connector).execute(request_id, latitude, longitude, message);
    }

    public void removePingResponse(PingApi.ServerResponseConnector connector, String response_id){
        new PingRemoveTask(connector).execute(response_id);
    }

    public void removePingRequest(PingApi.ServerResponseConnector connector, String response_id){
        new PingRequestRemoveTask(connector).execute(response_id);
    }

    public void acceptContact(PingApi.ServerResponseConnector connector, int request_id) {
        new AcceptContactTask(connector).execute(request_id);
    }

    private class LoginUserTask extends ServerTask<String, ServerResponse> {
        public LoginUserTask(PingApi.ServerResponseConnector requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(String... params) {
            String userEmail = params[0];
            String userPass = params[1];
            String gcmId = InstanceIdListenerService.getToken();
            ServerResponse response = pingApi.loginUser(userEmail, userPass, gcmId, "android");
            prefs.edit().putString("apiToken", response.getToken()).commit();
            return response;
        }
    }

    private class RegisterUserTask extends ServerTask<String, ServerResponse> {
        public RegisterUserTask(GenericServerConnector<ServerResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(String... params) {
            String userEmail = params[0];
            String userPass = params[1];
            String firstName = params[2];
            String lastName = params[3];
            String userName = params[4];
            String gcmId = InstanceIdListenerService.getToken();

            ServerResponse response = pingApi.registerUser(userEmail, userPass, gcmId, userName, firstName, lastName, "android");
            prefs.edit().putString("apiToken", response.getToken()).commit();
            return response;
        }

    }

    /**
     * Retrieves all user contacts using {@code PingService#apiToken} then updates {@link ContactManager#contactUserObjects} and provides a mechanism for
     * caller/requesting class to perform some behavior on response from the server.
     */
    private class GetContactsTask extends ServerTask<String, ContactsResponse> {
        public GetContactsTask(GenericServerConnector<ContactsResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ContactsResponse doInBackground(String... params) {
            ContactsResponse response = pingApi.getContacts();
            ContactUserObject[] contactUserObjects = response.getContactsUsers();
            ContactRequestObject[] contactRequestObjects = response.getContactRequests();
            if (contactUserObjects != null) {
                ContactManager.setContacts(contactUserObjects);
            }
            if (contactRequestObjects != null) {
                ContactManager.setRequests(contactRequestObjects);
            }

            return response;
        }
    }

    /**
     * Retrieves all user contacts using {@code PingService#apiToken} then updates {@link ContactManager#contactUserObjects}
     */
    private class GetContactsTaskNoResponse extends AsyncTask<Object, Object, Object> {

        @Override
        protected ContactsResponse doInBackground(Object... params) {
            if(pingApi == null){
                return null;
            }
            ContactsResponse response = pingApi.getContacts();
            ContactUserObject[] contactUserObjects = response.getContactsUsers();
            ContactRequestObject[] contactRequestObjects = response.getContactRequests();
            if (contactUserObjects != null) {
                ContactManager.setContacts(contactUserObjects);
            }
            if (contactRequestObjects != null) {
                ContactManager.setRequests(contactRequestObjects);
            }
            return response;
        }
    }

    /**
     * Retrieves all user contacts using {@code PingService#apiToken} then updates {@link ContactManager#contactUserObjects}
     */
    private class GetUsersTaskNoResponse extends AsyncTask<Object, Object, Object> {

        @Override
        protected PingApi.UsersResponse doInBackground(Object... params) {
            PingApi.UsersResponse response = pingApi.getAllUsers();
            User[] userObjects = response.getUsers();
            if (userObjects != null) {
                ContactManager.setUsers(userObjects);
            }
            return response;
        }
    }

    private class PingRequestTask extends ServerTask<String, ServerResponse> {
        public PingRequestTask(GenericServerConnector<ServerResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(String... params) {
            String targetId = params[0];
            String latitude = params[1];
            String longitude = params[2];
            String message = params[3];
            return pingApi.pingRequest(targetId, latitude, longitude, message);
        }

    }

    private class PingResponseTask extends ServerTask<String, ServerResponse> {
        public PingResponseTask(GenericServerConnector<ServerResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(String... params) {
            String requestId = params[0];
            String latitude = params[1];
            String longitude = params[2];
            String message = params[3];
            return pingApi.pingResponse(requestId, latitude, longitude, message);
        }

    }

    public static void bind(PingApiComponent component) {
        Context c = component.getContext();
        Intent intent = new Intent(c, PingService.class);
        PingServiceConnection serviceConnection = new PingServiceConnection(
                component);
        if(c.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)){
            connections.put(component, serviceConnection);
        }
    }

    public static void unBind(PingApiComponent component) {
        Context c = component.getContext();
        PingServiceConnection serviceConnection = connections.get(component);
        boolean isBound = c.bindService( new Intent(c, PingService.class), serviceConnection, Context.BIND_AUTO_CREATE );
        if(isBound) {
            c.unbindService(serviceConnection);
        }
    }

    private class ApiKeyInterceptor implements RequestInterceptor {

        @Override
        public void intercept(RequestFacade req) {
            String apiToken = prefs.getString("apiToken", null);
            if (apiToken != null)
                req.addHeader("apikey", apiToken);
        }

    }

    public void addContact(PingApi.ServerResponseConnector connector, String contact_id_or_email) {
        new AddContactTask(connector).execute(contact_id_or_email);
    }

    public void removeContact(ContactsResponseConnector connector, int user_id) {
        new RemoveContactTask(connector).execute(user_id);
    }

    private class RemoveContactTask extends ServerTask<Integer, ContactsResponse> {
        public RemoveContactTask(ContactsResponseConnector requestingClass) {
            super(requestingClass);
        }

        int userId;

        @Override
        protected ContactsResponse doInBackground(Integer... params) {
            userId = params[0];
            ContactsResponse response;
            response = pingApi.removeContact(userId);
            return response;
        }
    }

    private class AddContactTask extends ServerTask<String, ServerResponse> {
        public AddContactTask(GenericServerConnector<ServerResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(String... params) {
            String email = params[0];
            ServerResponse response = pingApi.addContact(email);
            return response;
        }

    }

    private class AcceptContactTask extends ServerTask<Integer, ServerResponse> {
        int request_id;

        public AcceptContactTask(PingApi.ServerResponseConnector requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(Integer... params) {
            request_id = params[0];
            ServerResponse response = pingApi.acceptContact(request_id);
            return response;
        }
    }

    private class GetUserTask extends ServerTask<String, PingApi.UserResponse> {

        public GetUserTask(GenericServerConnector<PingApi.UserResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected PingApi.UserResponse doInBackground(String... params) {
            String target_email = params[0];
            PingApi.UserResponse response = pingApi.getUser(target_email);
            return response;
        }
    }

    private class GetPingResponsesTask extends ServerTask<Object, PingApi.PingResponsesObject> {

        public GetPingResponsesTask(GenericServerConnector requestingClass) {
            super(requestingClass);
        }

        @Override
        protected PingApi.PingResponsesObject doInBackground(Object[] params) {
            PingApi.PingResponsesObject responsesObject = pingApi.getPingResponses();
            if (responsesObject != null) {
                PingManager.setPingResponses(responsesObject.getResponses());
            }
            return responsesObject;
        }
    }

    private class PingRemoveTask extends ServerTask<String, ServerResponse> {
        public PingRemoveTask(GenericServerConnector<ServerResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(String... params) {
            return pingApi.removePingResponse(params[0]);
        }

    }

    private class PingRequestRemoveTask extends ServerTask<String, ServerResponse> {
        public PingRequestRemoveTask(GenericServerConnector<ServerResponse> requestingClass) {
            super(requestingClass);
        }

        @Override
        protected ServerResponse doInBackground(String... params) {
            return pingApi.removePingRequest(params[0]);
        }

    }

    private class GetPingRequestsTask extends ServerTask<Object, PingApi.PingRequestsObject> {

        public GetPingRequestsTask(GenericServerConnector requestingClass) {
            super(requestingClass);
        }

        @Override
        protected PingApi.PingRequestsObject doInBackground(Object[] params) {
            PingApi.PingRequestsObject requestsObject = pingApi.getPingRequests();
            if (requestsObject != null) {
                PingManager.setPingRequests(requestsObject.getRequests());
            }
            return requestsObject;
        }
    }

    private class GetPingResponsesTaskNoResponse extends AsyncTask<Object, Object, Object> {

        @Override
        protected PingApi.PingResponsesObject doInBackground(Object[] params) {
            PingApi.PingResponsesObject responsesObject = pingApi.getPingResponses();
            if (responsesObject != null) {
                PingManager.setPingResponses(responsesObject.getResponses());
            }
            return responsesObject;
        }
    }

    private class GetPingRequestsTaskNoResponse extends AsyncTask<Object, Object, Object> {

        @Override
        protected PingApi.PingRequestsObject doInBackground(Object[] params) {
            PingApi.PingRequestsObject requestsObject = pingApi.getPingRequests();
            if (requestsObject != null) {
                PingManager.setPingRequests(requestsObject.getRequests());
            }
            return requestsObject;
        }
    }

}
