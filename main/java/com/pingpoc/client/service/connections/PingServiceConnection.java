package com.pingpoc.client.service.connections;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.pingpoc.client.service.PingService.PingBinder;
import com.pingpoc.client.service.interfaces.PingApiComponent;

public class PingServiceConnection implements ServiceConnection{
	PingApiComponent component;
	
		public PingServiceConnection(PingApiComponent component){
			super();
			this.component = component;
		}
	
       @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
        	PingBinder binder = (PingBinder) service;
            component.setService(binder.getService());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        	component.setService(null);
        	component = null;
        }
}