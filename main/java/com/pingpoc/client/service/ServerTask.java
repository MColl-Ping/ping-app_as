package com.pingpoc.client.service;

import android.os.AsyncTask;

import com.pingpoc.client.service.interfaces.PingApi.GenericServerConnector;

public abstract class ServerTask<T,K> extends AsyncTask<T, String, K>{
	GenericServerConnector<K> requestingClass;
	
	public ServerTask(GenericServerConnector<K> requestingClass){
		this.requestingClass = requestingClass;
	}
	
	@Override
	protected final void onPostExecute(K response){
		requestingClass.onServerResponse(response);
	}
}
