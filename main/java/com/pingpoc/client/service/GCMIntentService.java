package com.pingpoc.client.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.pingpoc.R;
import com.pingpoc.client.activities.MainActivity;
import com.pingpoc.client.data.User;
import com.pingpoc.client.service.interfaces.PingApi;
import com.pingpoc.client.service.interfaces.PingApiComponent;

import java.util.LinkedHashMap;

public class GCMIntentService extends GcmListenerService implements PingApiComponent {

    public static final String PING_REQUEST_ACTION = "PING_REQUEST_GCM";
    public static final String PING_RESPONSE_ACTION = "PING_RESPONSE_GCM";
    public static final String CONTACT_REQUEST_ACTION = "CONTACT_REQUEST_GCM";
    public static final String CONTACT_RESPONSE_ACTION = "CONTACT_RESPONSE_GCM";
    public static final String CONTACT_DELETED_ACTION = "CONTACT_DELETED_GCM";

    public static final int contact_request_intent = 0;
    public static final int ping_request_intent = 1;
    public static final int ping_response_intent = 2;

    PingService pingService;

    @Override
    public void onCreate() {
        PingService.bind(this);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addCategory(Intent.CATEGORY_DEFAULT);

        if (pingService == null) {
            return;
        }

        String message = data.getString("message");
        Gson gson = new Gson();
        GCMMessage gcmMessage = gson.fromJson(message, GCMMessage.class);
        LinkedHashMap gcmData = gcmMessage.data;

        Log.d(getClass().toString(), "From: " + from);
        Log.d(getClass().toString(), "Message: " + message);

        String notificationTitle = null;
        String notificationMessage = null;
        String notificationTag = null;
        String extraKey = null;
        String extraValue = null;
        int intentType = 3;

        switch (gcmMessage.type) {
            case "request":
                notificationTitle = "Ping Request";
                notificationMessage = (String) gcmData.get("username") + " wants to know your location";
                notificationTag = "request";
                intentType = ping_request_intent;
                intent.setAction(PING_REQUEST_ACTION);
                intent.putExtra("email", (String) gcmData.get("email"));
                intent.putExtra("request_id", ((Double) gcmData.get("id")).intValue());
                pingService.getPingRequests(new LocalResponseConnector(intent, this));
                break;
            case "response":
                notificationTitle = "Ping Response";
                notificationMessage = (String) gcmData.get("username") + " has sent you their location!";
                notificationTag = "response";
                extraKey = "response-id";
                extraValue = (String) gcmData.get("id");
                intentType = ping_response_intent;
                intent.setAction(PING_RESPONSE_ACTION);
                intent.putExtra("latitude", (Double) gcmData.get("locationx"));
                intent.putExtra("longitude", (Double) gcmData.get("locationy"));
                intent.putExtra("email", (String) gcmData.get("email"));
                intent.putExtra("username", (String) gcmData.get("username"));
                intent.putExtra("updated_at", (String) gcmData.get("updated_at"));
                pingService.getPingResponses(new LocalResponseConnector(intent, this));
                break;
            case "contact-request":
                notificationTitle = "Ping Contact Request";
                notificationMessage = ((LinkedTreeMap) gcmData.get("user")).get("username") + " wants to add you on Ping";
                notificationTag = "contact-request";
                intentType = contact_request_intent;
                intent.setAction(CONTACT_REQUEST_ACTION);
                pingService.getContacts(new LocalResponseConnector(intent, this));
//                intent.putExtra("request", new ContactRequestObject((Contact)gcmData.get("contact"), (User)gcmData.get("user")));
                break;
            case "contact-accepted":
                intent.setAction(CONTACT_RESPONSE_ACTION);
                pingService.getContacts(new LocalResponseConnector(intent, this));
                break;
            case "contact-deleted":
                intent.setAction(CONTACT_DELETED_ACTION);
                pingService.getContacts(new LocalResponseConnector(intent, this));
                break;
            case "handshake":
                return;
        }

        sendNotification(notificationTitle, notificationMessage, notificationTag, intentType, extraKey, extraValue);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private class GCMMessage {
        LinkedHashMap data;
        String type;
    }

    @Override
    public void setService(PingService service) {
        this.pingService = service;
        pingService.getPingResponses();
        pingService.getContacts();
    }

    @Override
    public Context getContext() {
        return this;
    }

    private class LocalResponseConnector implements PingApi.GenericServerConnector {

        Intent intent;
        Context context;

        LocalResponseConnector(Intent intent, Context context) {
            this.intent = intent;
            this.context = context;
        }

        @Override
        public void onServerResponse(Object response) {
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }

    private void sendNotification(String title, String text, String tag, int intentType, String extraKey, String extraValue) {
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("intent-type", intentType);
        if(extraKey != null && extraValue != null){
            resultIntent.putExtra(extraKey, extraValue);
        }
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon_round)
                        .setLargeIcon(BitmapFactory.decodeResource(getContext().getResources(),
                                R.drawable.icon_round))
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(resultPendingIntent)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX);

        int mNotificationId = 001;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(tag, mNotificationId, mBuilder.build());

    }

}
