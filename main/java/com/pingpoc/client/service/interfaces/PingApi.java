package com.pingpoc.client.service.interfaces;

import java.util.List;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.pingpoc.client.data.ContactRequestObject;
import com.pingpoc.client.data.ContactUserObject;
import com.pingpoc.client.data.PingRequest;
import com.pingpoc.client.data.PingResponse;
import com.pingpoc.client.data.User;

public interface PingApi {

    @FormUrlEncoded
    @POST("/users/register")
    ServerResponse registerUser(@Field("email") String email,
                                @Field("password") String password,
                                @Field("gcm_device") String gcm_device,
                                @Field("username") String username,
                                @Field("first_name") String first_name,
                                @Field("last_name") String last_name,
                                @Field("device_os") String device_os);

    @FormUrlEncoded
    @POST("/users/login")
    ServerResponse loginUser(@Field("email") String email,
                             @Field("password") String password,
                             @Field("gcm_device") String gcm_device,
                             @Field("device_os") String device_os);

    @GET("/users")
    UsersResponse getAllUsers();

    @GET("/users/{email}")
    UserResponse getUser(@Path("email") String email);

    // //TODO check server side that the user value passed name matches the
    // value stored for the api key
    // @POST("/users/update")
    // Response updateUser(@Body User user);

    // //TODO same here as above
    // @POST("/users/delete/{email}")
    // Response deleteUser(@Path("email")String email);

    @GET("/contacts")
    ContactsResponse getContacts();

    @FormUrlEncoded
    @POST("/contacts/add")
    ServerResponse addContact(@Field("target_email") String target_email);

    @FormUrlEncoded
    @POST("/contacts/respond")
    ServerResponse acceptContact(@Field("request_id") int request_id);

    @FormUrlEncoded
    @POST("/contacts/delete")
    ContactsResponse removeContact(@Field("target_id") int user_id);

    @FormUrlEncoded
    @POST("/ping-request")
    ServerResponse pingRequest(@Field("target_id") String targetId,
                               @Field("locationx") String latitude,
                               @Field("locationy") String longitude,
                               @Field("message") String message);

    @FormUrlEncoded
    @POST("/ping-response")
    ServerResponse pingResponse(@Field("request_id") String targetId,
                                @Field("locationx") String latitude,
                                @Field("locationy") String longitude,
                                @Field("message") String message);

    @GET("/ping-response")
    PingResponsesObject getPingResponses();

    @GET("/ping-request")
    PingRequestsObject getPingRequests();

    @FormUrlEncoded
    @POST("/ping-response/delete")
    ServerResponse removePingResponse(@Field("request_id") String pingResponseId);

    @FormUrlEncoded
    @POST("/ping-request/delete")
    ServerResponse removePingRequest(@Field("request_id") String pingRequestId);

    public class ServerResponse {
        int error;
        String message;
        String token;

        public int getError() {
            return error;
        }

        public String getMessage() {
            return message;
        }

        public String getToken() {
            return token;
        }

        public boolean hasError() {
            if (error == 0)
                return false;
            return true;
        }
    }

    public class UserResponse extends ServerResponse {
        User user;

        public User getUser() {
            return user;
        }
    }

    public class UsersResponse extends ServerResponse {
        User[] users;

        public User[] getUsers() {
            return users;
        }
    }

    public class ContactsResponse extends ServerResponse {
        ContactUserObject contacts[];
        ContactRequestObject requests[];

        public ContactUserObject[] getContactsUsers() {
            return contacts;
        }

        public ContactRequestObject[] getContactRequests() {
            return requests;
        }
    }

    public class PingResponsesObject extends ServerResponse {
        PingResponse responses[];

        public PingResponse[] getResponses() {
            return responses;
        }
    }

    public class PingRequestsObject extends ServerResponse {
        PingRequest requests[];

        public PingRequest[] getRequests() {
            return requests;
        }
    }


    public interface GenericServerConnector<K> {
        public void onServerResponse(K response);
    }

    public interface ServerResponseConnector extends
            GenericServerConnector<ServerResponse> {

    }

    public interface ContactsResponseConnector extends
            GenericServerConnector<ContactsResponse> {

    }

    public interface UserResponseConnector extends GenericServerConnector<UserResponse> {

    }

    public interface PingResponseConnector extends GenericServerConnector<PingResponsesObject> {

    }

}
