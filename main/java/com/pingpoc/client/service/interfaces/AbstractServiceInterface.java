package com.pingpoc.client.service.interfaces;

import com.pingpoc.client.service.PingService;

public interface AbstractServiceInterface{
	PingService getService();
}