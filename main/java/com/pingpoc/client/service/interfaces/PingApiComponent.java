package com.pingpoc.client.service.interfaces;

import android.content.Context;

import com.pingpoc.client.service.PingService;

public interface PingApiComponent {
	public void setService(PingService service);
	public Context getContext();
}
